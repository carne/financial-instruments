package instrument.computational;

import instrument.model.ComputationResult;
import instrument.model.Instrument;
import instrument.service.InstrumentConverter;
import instrument.service.StringInstrumentConverter;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class TenNewInstrumentByDateComputationalTest {

    @Test
    public void test() {
        InstrumentComputational computational = new TenNewInstrumentByDateComputational();
        getInstruments().forEach(computational::process);
        ComputationResult result = computational.result();
        String simpleClassName = TenNewInstrumentByDateComputational.class.getSimpleName();
        assertEquals(simpleClassName, result.getComputationClassName());
        String treeSetClassName = TreeSet.class.getSimpleName();
        assertEquals(treeSetClassName, result.getResultObjectClassName());
        TreeSet<Instrument> instruments = (TreeSet<Instrument>) result.getResultObject();
        assertEquals(10, instruments.size());
        LocalDate firstDate = LocalDate.of(2017, 11, 23);
        LocalDate lastDate = LocalDate.of(2014, 11, 19);
        assertEquals(firstDate, instruments.first().getDate());
        assertEquals(lastDate, instruments.last().getDate());
    }

    List<Instrument> getInstruments() {
        InstrumentConverter<String> converter = new StringInstrumentConverter();

        return asList(
                converter.convert("INSTRUMENT,14-Nov-2014,9.388888889"),
                converter.convert("INSTRUMENT,15-Nov-2014,100.388888889"),
                converter.convert("INSTRUMENT,17-Nov-2014,9.39586823"),
                converter.convert("INSTRUMENT,18-Nov-2014,9.386385692"),
                converter.convert("INSTRUMENT,19-Nov-2014,9.38427464"),
                converter.convert("INSTRUMENT,21-Nov-2014,9.382934587"),
                converter.convert("INSTRUMENT,22-Nov-2014,9.382934587"),
                converter.convert("INSTRUMENT,23-Nov-2014,9.382934587"),
                converter.convert("INSTRUMENT,14-Nov-2015,9.388888889"),
                converter.convert("INSTRUMENT,21-Nov-2015,9.382934587"),
                converter.convert("INSTRUMENT,19-Nov-2015,9.38427464"),
                converter.convert("INSTRUMENT,22-Nov-2015,9.382934587"),
                converter.convert("INSTRUMENT,15-Nov-2015,100.388888889"),
                converter.convert("INSTRUMENT,18-Nov-2015,9.386385692"),
                converter.convert("INSTRUMENT,17-Nov-2015,9.39586823"),
                converter.convert("INSTRUMENT,23-Nov-2015,9.382934587"),
                converter.convert("INSTRUMENT,17-Nov-2016,9.39586823"),
                converter.convert("INSTRUMENT,23-Nov-2016,9.382934587"),
                converter.convert("INSTRUMENT,17-Nov-2017,9.39586823"),
                converter.convert("INSTRUMENT,23-Nov-2017,9.382934587")
        );
    }
}