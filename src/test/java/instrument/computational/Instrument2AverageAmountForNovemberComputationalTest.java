package instrument.computational;

import instrument.model.ComputationResult;
import instrument.service.InstrumentConverter;
import instrument.model.Instrument;
import instrument.service.StringInstrumentConverter;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class Instrument2AverageAmountForNovemberComputationalTest {
    private BigDecimal result;

    @Test
    public void test() {
        Instrument2AverageAmountForNovemberComputational computational = new Instrument2AverageAmountForNovemberComputational();
        getInstruments().forEach(computational::process);
        ComputationResult result = computational.result();

        assertEquals(Instrument2AverageAmountForNovemberComputational.class.getSimpleName(), result.getComputationClassName());
        assertEquals(BigDecimal.class.getSimpleName(), result.getResultObjectClassName());
        assertEquals(new BigDecimal("9.388"), result.getResultObject());

    }

    List<Instrument> getInstruments() {
        InstrumentConverter<String> converter = new StringInstrumentConverter();

        return asList(
                converter.convert("INSTRUMENT2,14-Nov-2014,9.388888889"),
                converter.convert("INSTRUMENT2,15-Nov-2014,100.388888889"),
                converter.convert("INSTRUMENT2,17-Nov-2014,9.39586823"),
                converter.convert("INSTRUMENT2,18-Nov-2014,9.386385692"),
                converter.convert("INSTRUMENT2,19-Nov-2014,9.38427464"),
                converter.convert("INSTRUMENT2,20-Nov-2014,9.382934587"),
                converter.convert("INSTRUMENT1,20-Nov-2014,9.382934587"),
                converter.convert("INSTRUMENT3,20-Nov-2014,9.382934587")
        );
    }

}