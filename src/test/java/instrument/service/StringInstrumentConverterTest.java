package instrument.service;

import instrument.model.Instrument;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringInstrumentConverterTest {

    @Test
    public void convert() {
        StringInstrumentConverter converter = new StringInstrumentConverter();
        Instrument instrument = converter.convert("INSTRUMENT1,01-Jan-1996,2.4655");
        assertEquals("INSTRUMENT1", instrument.getName());
        assertEquals("1996-01-01", instrument.getDate().toString());
        assertEquals("2.4655", instrument.getAmount().toString());
    }

}