package instrument.service;

import instrument.model.ComputationResult;
import instrument.model.Instrument;
import instrument.computational.InstrumentComputational;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class ParallelInstrumentComputationalServiceTest {

    private final static AtomicInteger ATOMIC_INTEGER = new AtomicInteger();
    private final static Map<Integer, ComputationResult> RESULT_MAP = new ConcurrentHashMap<>();
    StringInstrumentConverter converter = new StringInstrumentConverter();

    @Test
    public void testService() {
        List<InstrumentComputational> computationalList =
                asList(getDummyInstrumentComputational(), getDummyInstrumentComputational());
        InstrumentComputationalService service =
                new ParallelInstrumentComputationalService(computationalList, getDisplayResultService());
        Instrument instrument = converter.convert("INSTRUMENT1,01-Jan-1996,2.4655");
        service.process(instrument);
        service.finish();
        assertEquals(2, RESULT_MAP.size());
        RESULT_MAP.forEach((s, computationResult) -> {

            Instrument processedInstrument = (Instrument) computationResult.getResultObject();
            assertEquals("INSTRUMENT1", processedInstrument.getName());
            assertEquals("1996-01-01", processedInstrument.getDate().toString());
            assertEquals("2.4655", processedInstrument.getAmount().toString());
        });

    }

    private InstrumentComputational getDummyInstrumentComputational() {
       return new InstrumentComputational() {
            private Instrument instrument;
            @Override
            public Predicate<Instrument> filter() {
                return instrument -> "INSTRUMENT1".equals(instrument.getName());
            }

            @Override
            public void handle(Instrument instrument) {
                this.instrument = instrument;
            }

           @Override
           public ComputationResult result() {
               return new ComputationResult(this.getClass().getSimpleName(), instrument.getClass().getSimpleName(), instrument);
           }


       };


    }

    private HandlerResult getDisplayResultService() {
        return computationResult -> {
            int i = ATOMIC_INTEGER.incrementAndGet();
            RESULT_MAP.put(i, computationResult);
        };
    }



}