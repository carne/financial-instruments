package instrument.service;

import instrument.model.Instrument;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class InstrumentFileReaderImplTest {

    @Test
    public void testReadFile() {
        InstrumentConverter<String> converter = new StringInstrumentConverter();
        List<Instrument> instruments = new ArrayList<>();
        try (InstrumentFileReaderImpl fileReader =
                    new InstrumentFileReaderImpl("src/test/resources/Financial_instruments.txt", converter)) {
           fileReader.forEachRemaining(instruments::add);
        } catch (Exception e) {
            new RuntimeException(e);
        }
        assertEquals(14826, instruments.size());
    }

}