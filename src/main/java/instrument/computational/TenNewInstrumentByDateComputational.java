package instrument.computational;

import instrument.model.ComputationResult;
import instrument.model.Instrument;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

@Component
public class TenNewInstrumentByDateComputational implements InstrumentComputational {
    private final Set<String> excludeSet = new HashSet<>(Arrays.asList("INSTRUMENT1", "INSTRUMENT2", "INSTRUMENT3"));
    TreeSet<Instrument> setInstrument = new TreeSet<>(Comparator.comparing(Instrument::getDate).reversed());

    @Override
    public Predicate<Instrument> filter() {
        return instrument -> !excludeSet.contains(instrument.getName())
                && instrument.getDate().getDayOfWeek() != DayOfWeek.SATURDAY
                && instrument.getDate().getDayOfWeek() != DayOfWeek.SUNDAY;
    }

    @Override
    public void handle(Instrument instrument) {
        if (setInstrument.size() < 10) {
            setInstrument.add(instrument);
        }
        else if (instrument.getDate().isAfter(setInstrument.last().getDate())) {
            setInstrument.remove(setInstrument.last());
            setInstrument.add(instrument);
        }
    }

    @Override
    public ComputationResult result() {
        return createComputationResult(setInstrument);
    }
}
