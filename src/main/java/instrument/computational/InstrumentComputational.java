package instrument.computational;

import instrument.model.ComputationResult;
import instrument.model.Instrument;

import java.util.function.Predicate;

public interface InstrumentComputational {

    Predicate<Instrument> filter();

    void handle(Instrument instrument);

    default void process(Instrument instrument) {
        if (filter().test(instrument)) {
            handle(instrument);
        }
    }

    ComputationResult result();

    default ComputationResult createComputationResult(Object o) {
        return new ComputationResult(this.getClass().getSimpleName(), o.getClass().getSimpleName(), o);
    }
}
