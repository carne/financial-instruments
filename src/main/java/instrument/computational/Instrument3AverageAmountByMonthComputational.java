package instrument.computational;

import instrument.model.ComputationResult;
import instrument.model.Instrument;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

@Component
public class Instrument3AverageAmountByMonthComputational implements InstrumentComputational {
    Map<String, Pair<Integer, BigDecimal>> computationMap = new HashMap<>();

    @Override
    public Predicate<Instrument> filter() {
        return instrument -> "INSTRUMENT3".equals(instrument.getName());
    }

    @Override
    public void handle(Instrument instrument) {
        LocalDate date = instrument.getDate();
        String key = date.getMonth() + "-" + date.getYear();
        Pair<Integer, BigDecimal> pair = computationMap.get(key);
        if (isNull(pair)) {
            computationMap.put(key, new Pair<>(1, instrument.getAmount()));
        } else {
            Integer count = pair.getFirst();
            BigDecimal totalAmount = pair.getSecond();
            totalAmount = totalAmount.add(instrument.getAmount());
            ++count;
            pair.setFirst(count);
            pair.setSecond(totalAmount);
            computationMap.put(key, pair);
        }
    }

    @Override
    public ComputationResult result() {
        List<Pair<String, BigDecimal>> resultList = computationMap.entrySet().stream()
                .map(entry -> {
                    Pair<Integer, BigDecimal> pair = entry.getValue();
                    Integer count = pair.getFirst();
                    BigDecimal totalAmount = pair.getSecond();
                    BigDecimal result =
                            count == 0
                                    ? BigDecimal.ZERO
                                    : totalAmount.divide(new BigDecimal(count), 3, RoundingMode.CEILING);
                    return new Pair<>(entry.getKey(), result);
                }).collect(toList());
        return createComputationResult(resultList);
    }

    private static class Pair<FIRST, SECOND> {
        private FIRST first;
        private SECOND second;

        public Pair(FIRST first, SECOND second) {
            this.first = first;
            this.second = second;
        }

        public FIRST getFirst() {
            return first;
        }

        public void setFirst(FIRST first) {
            this.first = first;
        }

        public SECOND getSecond() {
            return second;
        }

        public void setSecond(SECOND second) {
            this.second = second;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "first=" + first +
                    ", second=" + second +
                    '}';
        }
    }


}
