package instrument.computational;

import instrument.model.Instrument;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.util.function.Predicate;

@Component
public class Instrument1AverageAmountComputational extends AbstractAverageAmountComputational {

    @Override
    public Predicate<Instrument> filter() {
        return instrument -> "INSTRUMENT1".equals(instrument.getName())
                && instrument.getDate().getDayOfWeek() != DayOfWeek.SATURDAY
                && instrument.getDate().getDayOfWeek() != DayOfWeek.SUNDAY;
    }
}
