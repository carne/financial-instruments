package instrument.computational;

import instrument.model.Instrument;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.function.Predicate;

import static java.time.Month.DECEMBER;
import static java.time.Month.OCTOBER;

@Component
public class Instrument2AverageAmountForNovemberComputational extends AbstractAverageAmountComputational {
    @Override
    public Predicate<Instrument> filter() {
        return instrument -> "INSTRUMENT2".equals(instrument.getName())
                && instrument.getDate().isAfter(LocalDate.of(2014, OCTOBER, 31))
                && instrument.getDate().isBefore(LocalDate.of(2014, DECEMBER, 1))
                && instrument.getDate().getDayOfWeek() != DayOfWeek.SATURDAY
                && instrument.getDate().getDayOfWeek() != DayOfWeek.SUNDAY;
    }
}
