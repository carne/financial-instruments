package instrument.computational;

import instrument.model.ComputationResult;
import instrument.model.Instrument;

import java.math.BigDecimal;
import java.math.RoundingMode;


public abstract class AbstractAverageAmountComputational implements InstrumentComputational {
    private int count = 0;
    private BigDecimal totalAmount = new BigDecimal("0.0");


    @Override
    public void handle(Instrument instrument) {
        totalAmount = totalAmount.add(instrument.getAmount());
        ++count;
    }

    @Override
    public ComputationResult result() {
        BigDecimal result =
                count == 0 ? BigDecimal.ZERO : totalAmount.divide(new BigDecimal(count), 3, RoundingMode.CEILING);
        return createComputationResult(result);
    }
}
