package instrument.service;

import instrument.model.ComputationResult;
import instrument.model.Instrument;
import instrument.computational.InstrumentComputational;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;

import java.util.List;

import static instrument.model.JmsWrapperInstrument.finishWrapper;
import static instrument.model.JmsWrapperInstrument.processWrapper;

public class JmsProducerParallelInstrumentComputationalService extends ParallelInstrumentComputationalService {
    private final JmsTemplate jmsTemplate;
    private final String topicName;
    public JmsProducerParallelInstrumentComputationalService(List<InstrumentComputational> computationals, HandlerResult handlerResult, JmsTemplate jmsTemplate, String topicName) {

        super(computationals,handlerResult);
        this.jmsTemplate = jmsTemplate;
        this.topicName = topicName;

    }

    @Override
    public void process(Instrument instrument) {
        jmsTemplate.convertAndSend(topicName, processWrapper(instrument));
        super.process(instrument);
    }

    @Override
    public void finish() {
        jmsTemplate.convertAndSend(topicName, finishWrapper());
        super.finish();
    }

    @JmsListener(destination = "${instrument.queue}")
    public void listenFinishQueue(ComputationResult computationResult) {
        handlerResult.handle(computationResult);
    }
}
