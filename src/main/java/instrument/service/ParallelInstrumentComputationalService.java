package instrument.service;

import instrument.model.Instrument;
import instrument.computational.InstrumentComputational;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class ParallelInstrumentComputationalService implements InstrumentComputationalService {
    protected final List<InstrumentComputational> computationals;
    protected final HandlerResult handlerResult;
    private final ExecutorService executorService ;
    public ParallelInstrumentComputationalService(List<InstrumentComputational> computationals, HandlerResult handlerResult) {
        this.computationals = computationals;
        this.handlerResult = handlerResult;
        this.executorService = Executors.newFixedThreadPool(computationals.size());

    }

    @Override
    public void process(Instrument instrument) {
        runAndAwaitAllComputation(instrumentComputational -> instrumentComputational.process(instrument));
    }

    @Override
    public void finish() {
        runAndAwaitAllComputation(instrumentComputational -> handlerResult.handle(instrumentComputational.result()));
    }

    private void runAndAwaitAllComputation(Consumer<InstrumentComputational> consumer) {
        CountDownLatch countDownLatch = new CountDownLatch(computationals.size());
        computationals.forEach(instrumentComputational -> {
            executorService.submit(() -> {
                try {
                    consumer.accept(instrumentComputational);
                }
                finally {
                    countDownLatch.countDown();
                }
            });
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            throw  new RuntimeException(e);
        }
    }
}
