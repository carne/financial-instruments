package instrument.service;

import instrument.model.Instrument;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class StringInstrumentConverter implements InstrumentConverter<String> {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-uuuu");
    @Override
    public Instrument convert(String s) {
        Objects.requireNonNull(s);
        String[] splitArray = s.split(",");
        if (splitArray.length < 3) {
            throw new IllegalArgumentException(String.format("bad input string [%s]", s));
        }
        String name = splitArray[0];
        LocalDate date = LocalDate.parse(splitArray[1], formatter);
        BigDecimal amount = new BigDecimal(splitArray[2]);
        return new Instrument(name, date, amount);
    }
}
