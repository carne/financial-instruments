package instrument.service;

import instrument.model.Instrument;

import java.util.Iterator;

public interface InstrumentFileReader extends Iterator<Instrument>, AutoCloseable {
}
