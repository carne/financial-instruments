package instrument.service;

import instrument.model.Instrument;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class InstrumentFileReaderImpl implements InstrumentFileReader {
    private final FileInputStream inputStream;
    private final Scanner scanner;
    private final InstrumentConverter<String> converter;

    public InstrumentFileReaderImpl(String path, InstrumentConverter<String> converter) throws IOException {
        this.converter = converter;
        inputStream = new FileInputStream(path);
        scanner = new Scanner(inputStream, "UTF-8");
    }

    @Override
    public boolean hasNext() {
        if (scanner.ioException() != null) {
            throw new RuntimeException(scanner.ioException());
        }
        return scanner.hasNext();
    }

    @Override
    public Instrument next() {
        return converter.convert(scanner.next());
    }

    @Override
    public void close() throws Exception {
        if (inputStream != null) {
            inputStream.close();
        }
        scanner.close();
    }
}
