package instrument.service;

import instrument.computational.InstrumentComputational;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;
import java.util.stream.Stream;

@Service
public class InstrumentConfigService {
    @Value("${instrument.included:*}")
    String included;
    @Value("${instrument.excluded:}")
    String excluded;

    public boolean isIncludeComputation(InstrumentComputational instrumentComputational) {
        Predicate<String> computationalClassExistsInList = s -> s.equals(instrumentComputational.getClass().getSimpleName());
        if (included.startsWith("*")) {
            if(!excluded.isEmpty()) {
                return Stream.of(excluded.split(","))
                        .noneMatch(computationalClassExistsInList);
            }
            return true;
        }
        return Stream.of(included.split(",")).anyMatch(computationalClassExistsInList);
    }

}
