package instrument.service;


import instrument.model.Instrument;

public interface InstrumentComputationalService {
    void process(Instrument instrument);
    void finish();
}
