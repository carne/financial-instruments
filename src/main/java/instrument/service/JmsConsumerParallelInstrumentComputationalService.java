package instrument.service;

import instrument.model.JmsWrapperInstrument;
import instrument.computational.InstrumentComputational;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;

import java.util.List;

import static instrument.model.JmsWrapperInstrument.Event.FINISH;
import static instrument.model.JmsWrapperInstrument.Event.PROCESS;

public class JmsConsumerParallelInstrumentComputationalService extends ParallelInstrumentComputationalService {
    private final JmsTemplate jmsTemplate;
    private final String queueName;

    public JmsConsumerParallelInstrumentComputationalService(List<InstrumentComputational> computationals,
                                                             HandlerResult handlerResult,
                                                             JmsTemplate jmsTemplate,
                                                             String queueName) {

        super(computationals, handlerResult);
        this.jmsTemplate = jmsTemplate;
        this.queueName = queueName;

    }

    @JmsListener(destination = "${instrument.topic}")
    public void listenFinishQueue(JmsWrapperInstrument wrapperInstrument) {
        if (wrapperInstrument.getEvent() == PROCESS) {
            super.process(wrapperInstrument.getInstrument());
        }
        if (wrapperInstrument.getEvent() == FINISH) {
            computationals.stream()
                    .map(InstrumentComputational::result)
                    .forEach(computationResult -> jmsTemplate.convertAndSend(queueName, computationResult));
        }
    }
}
