package instrument.service;

import instrument.model.ComputationResult;
import org.springframework.stereotype.Service;

@Service
public class ConsoleHandlerResult implements HandlerResult {

    @Override
    public void handle(ComputationResult computationResult) {
        System.out.println(computationResult.toString());
    }
}
