package instrument.service;

import instrument.model.ComputationResult;

public interface HandlerResult {
    void handle(ComputationResult computationResult);
}
