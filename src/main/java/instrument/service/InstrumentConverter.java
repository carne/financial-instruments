package instrument.service;

import instrument.model.Instrument;

public interface InstrumentConverter<T> {
    Instrument convert(T t);
}
