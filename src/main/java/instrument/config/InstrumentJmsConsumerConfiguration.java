package instrument.config;

import instrument.service.HandlerResult;
import instrument.service.InstrumentComputationalService;
import instrument.service.InstrumentConfigService;
import instrument.service.JmsConsumerParallelInstrumentComputationalService;
import instrument.computational.InstrumentComputational;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.core.JmsTemplate;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Configuration
@Profile("CONSUMER")
public class InstrumentJmsConsumerConfiguration {
    @Value("${instrument.queue}")
    private String queueName;

    @Bean
    InstrumentComputationalService jmsProducerComputationalService(List<InstrumentComputational> computationals,
                                                                   HandlerResult handlerResult,
                                                                   JmsTemplate jmsTemplate,
                                                                   InstrumentConfigService configService) {
        List<InstrumentComputational> computationalList = computationals
                .stream()
                .filter(configService::isIncludeComputation).collect(toList());
        return new JmsConsumerParallelInstrumentComputationalService(computationalList, handlerResult, jmsTemplate, queueName);
    }
}
