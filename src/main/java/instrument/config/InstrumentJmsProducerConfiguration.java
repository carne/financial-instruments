package instrument.config;

import instrument.service.HandlerResult;
import instrument.service.InstrumentConfigService;
import instrument.service.JmsProducerParallelInstrumentComputationalService;
import instrument.computational.InstrumentComputational;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.core.JmsTemplate;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Configuration
@Profile("PRODUCER")
public class InstrumentJmsProducerConfiguration {
    @Value("${instrument.topic}")
    private String topicName;

    @Bean
    JmsProducerParallelInstrumentComputationalService jmsProducerComputationalService(List<InstrumentComputational> computationals,
                                                                                      HandlerResult handlerResult,
                                                                                      JmsTemplate jmsTemplate,
                                                                                      InstrumentConfigService configService) {
        List<InstrumentComputational> computationalList = computationals
                .stream()
                .filter(configService::isIncludeComputation).collect(toList());
        return new JmsProducerParallelInstrumentComputationalService(computationalList, handlerResult, jmsTemplate, topicName);
    }
}
