package instrument.config;

import instrument.service.HandlerResult;
import instrument.service.ParallelInstrumentComputationalService;
import instrument.computational.InstrumentComputational;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.List;

@Configuration
@Profile("SINGLE")
public class SingleConfiguration {

    @Bean
    ParallelInstrumentComputationalService computationalService (List<InstrumentComputational> computationals, HandlerResult handlerResult){
       return new ParallelInstrumentComputationalService(computationals, handlerResult);
    }
}
