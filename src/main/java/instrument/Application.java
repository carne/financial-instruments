package instrument;

import instrument.service.InstrumentComputationalService;
import instrument.service.InstrumentFileReader;
import instrument.service.InstrumentFileReaderImpl;
import instrument.service.StringInstrumentConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import java.io.IOException;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Value("${instrument.filePath}")
    String filePath;


    InstrumentFileReader instrumentFileReader () throws IOException {
        return new InstrumentFileReaderImpl(filePath, new StringInstrumentConverter());
    }

    @Bean
    @Profile({"SINGLE", "PRODUCER"})
    CommandLineRunner runner(InstrumentComputationalService instrumentComputationalService) {
        return args -> {
            InstrumentFileReaderImpl instrumentFileReader = new InstrumentFileReaderImpl(filePath, new StringInstrumentConverter());
            instrumentFileReader.forEachRemaining(instrumentComputationalService::process);
            instrumentComputationalService.finish();
        };
    }

    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
}
