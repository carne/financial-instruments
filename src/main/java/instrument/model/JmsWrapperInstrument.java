package instrument.model;

public class JmsWrapperInstrument {
    private Instrument instrument;
    private Event event;

    public JmsWrapperInstrument() {
    }

    public JmsWrapperInstrument(Instrument instrument, Event event) {
        this.instrument = instrument;
        this.event = event;
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public Event getEvent() {
        return event;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public enum Event {
        PROCESS, FINISH
    }

    public static JmsWrapperInstrument processWrapper(Instrument instrument) {
        return new JmsWrapperInstrument(instrument,Event.PROCESS);
    }
    public static JmsWrapperInstrument finishWrapper() {
        return new JmsWrapperInstrument(null,Event.FINISH);
    }
}
