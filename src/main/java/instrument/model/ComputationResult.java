package instrument.model;

public class ComputationResult {

    private String computationClassName;
    private String resultObjectClassName;
    private Object resultObject;

    public ComputationResult() {
    }

    public ComputationResult(String computationClassName, String resultObjectClassName, Object resultObject) {
        this.computationClassName = computationClassName;
        this.resultObjectClassName = resultObjectClassName;
        this.resultObject = resultObject;
    }

    public String getComputationClassName() {
        return computationClassName;
    }

    public String getResultObjectClassName() {
        return resultObjectClassName;
    }

    public Object getResultObject() {
        return resultObject;
    }

    public void setComputationClassName(String computationClassName) {
        this.computationClassName = computationClassName;
    }

    public void setResultObjectClassName(String resultObjectClassName) {
        this.resultObjectClassName = resultObjectClassName;
    }

    public void setResultObject(Object resultObject) {
        this.resultObject = resultObject;
    }

    @Override
    public String toString() {
        return "ComputationResult{" +
                "computationClassName='" + computationClassName + '\'' +
                ", resultObjectClassName='" + resultObjectClassName + '\'' +
                ", resultObject=" + resultObject +
                '}';
    }
}
